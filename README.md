# May I?

A role-to-permissions compiler and fail-fast permissions checker

## Installation

`npm install git@bitbucket.org:digimondo/may-i.git`

## Usage 

### Terminology

#### Role

A list of roles and permissions.

```json
[
    {"role": "default"},
    {"role": "user"},
    {"cat/create": true},
    {"cat/20/view": false},
]
```

##### Default role

The default role disallows everything. It is useful to put this on the top of every other role.

```json
[
    {"*": false}
]
```

#### Permission

A scope with an allowance.

```json
{"cat/create": true}
```

#### Scope

Something that you can give permission to. 

`cat/*/view`, `cat`, `cat/10/edit`

A scope is written like a path, separating the segments with a forward slash `/`.

#### Query

Can be matched to a scope. This is used to check a list of permissions for a specific allowance, such as `cat/10/view`. 

Queries are matched against each scope in a permissions list. 

##### How queries are matched against scopes

Both scope and query are split into their segments, which are then compared to each other in order.

\# | Scope        | Query         | Explanation
---|--------------|---------------|-------------------------------------------------------------------------------------
   | `cat/*/view` | `cat/10/view` | Example
 1 | `cat`        | `cat`         | Both equal, so we continue to the next segment
 2 | `*`          | `10`          | A wildcard (`*`) always matches everything that is in the respective query segment
 3 | `view`       | `view`        | Both equal
   |              |               | Return `true`
   
\# | Scope        | Query         | Explanation
---|--------------|---------------|-------------------------------------------------------------------------------------
   | `cat/*/view` | `cat/create`  | Return `false` because the query is less specific than the scope

\# | Scope        | Query         | Explanation
---|--------------|---------------|-------------------------------------------------------------------------------------
   | `cat/create` | `cat/10/view` | Example
 1 | `cat`        | `cat`         | Both equal
 2 | `create`     | `10`          | Not equal
~~3~~ | ~~`null`~~| ~~`view`~~    | ~~Skipped~~
   |              |               | Return `false`
   
   
#### Query params

May I? allows you to insert params into your query. Params are segments that were prefixed with a `:`.

If you pass a param-map to the `check` function, it will insert the values from the map into the query.
 
##### Example

`cat/:catId/photo/:photoId/view`

```json
{
    "catId": 10,
    "photoId": 20
}
```

Result: `cat/10/photo/20/view`

### API

```javascript
let mayI = require('may-i')
```

#### `mayI.compile(role, roleProvider)`

Compiles a role to a flat role.
 
A flat role is a role that doesn't contain any other roles.

If it finds a required role, it calls the `roleProvider` with the name and will insert the returned role into the 
resulting role.

##### Arguments

 * `role`: A role set to reduce to a list of permissions
 * `roleProvider`: A function that takes a role name and returns a role

##### Returns

A flat role

#### `mayI.check(role, query, params)`

Checks if the given _flat_ role allows the query.

The roles permissions will be checked bottom-to-top. The first permission whose scope matches the query will be 
returned.
