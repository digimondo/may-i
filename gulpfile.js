const gulp = require("gulp");
const babel = require("gulp-babel");
const mocha = require('gulp-mocha');
require('babel-core/register');

gulp.task("default", function () {
    return gulp.src("lib/*.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

gulp.task("test", function() {
    return gulp.src("test/*.js")
        .pipe(mocha())
});
