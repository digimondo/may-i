"use strict";
import _ from 'lodash';

export function compile(role, roleProvider, options) {
    var roleSource = false;
    if (options) {
        roleSource = options.roleSource || false;
    }
    var output = [];

    _.forEach(role, (roleElement) => {
        if (roleElement.role) {
            var subRole = compile(roleProvider(roleElement.role), roleProvider);
            roleSource && _.map(subRole, (entry) =>  {
                if (!entry.roleSource) {
                    entry.roleSource = [];
                }
                entry.roleSource.push(roleElement.role);
                return entry;
            });
            output = _.concat(output, subRole);
        } else {
            output.push(roleElement);
        }
    });

    return output;
}
