import _ from 'lodash';
import {matches} from './check';

function lint(role) {
    var res = {
        errors: []
    };

    //duplicate check
    for (var i = 0; i < role.length; i++) {
        var perm1 = role[i];
        var scope1 = _.keys(perm1)[0];
        for (var j = i + 1; j < role.length; j++) {
            var perm2 = role[j];
            var scope2 = _.keys(perm2)[0];
            if (matches(perm2, perm1)) {
                res.errors.push({
                    message: "Scope is overwritten by other scope",
                    overwrittenScope: scope1,
                    overwritingScope: scope2
                });
            }
        }
    }

    return res;
}

module.exports = {
    lint
};