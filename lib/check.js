"use strict";

import _ from 'lodash';

/**
 * Checks a role if it allows the given query
 * @param role a list of permissions to check query against
 * @param query the query to the role
 * @param params additional params that the query will be matched with
 * @returns {Boolean} whether the given role allows the query, or null if no matching permission was found
 */
function check(role, query, params) {
    var compiledQuery = check.compileQuery(query, params);
    var result = null;
    _.forEachRight(role, (permission) => {
        var scope = _.keys(permission)[0];
        var allow = permission[scope];
        if (check.matches(scope, compiledQuery)) {
            result = allow;
            return false;
        }
    });
    return result;
}

check.check = check;

check.matches = function(scope, compiledQuery) {
    scope = _.split(scope, '/');
    compiledQuery = _.split(compiledQuery, '/');
    if (compiledQuery.length < scope.length) {
        // not specific enough
        return false;
    }

    for (var i = 0; i < compiledQuery.length; i++) {
        var scopePart = scope[i];
        var queryPart = compiledQuery[i];
        if (scopePart === '*') {
            // if wildcard, we don't look at what is inside the query
            continue;
        }
        if (!scopePart && !!queryPart) {
            // if query is more specific than the scope, they match
            return true;
        }
        if (scopePart !== queryPart) {
            // if scope and query are not equal at this point, they don't match
            return false;
        }
    }
    // if it went through all parts without returning, it matches
    return true;
};

check.compileQuery = function(query, params) {
    if (!params) {
        return query;
    }
    _.forEach(params, (value, key) => {
        key = _.escapeRegExp(':' + key);
        query = query.replace(new RegExp(key, 'g'), value);
    });
    return query;
};

module.exports = check;
