"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.compile = compile;

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function compile(role, roleProvider, options) {
    var roleSource = false;
    if (options) {
        roleSource = options.roleSource || false;
    }
    var output = [];

    _lodash2.default.forEach(role, function (roleElement) {
        if (roleElement.role) {
            var subRole = compile(roleProvider(roleElement.role), roleProvider);
            roleSource && _lodash2.default.map(subRole, function (entry) {
                if (!entry.roleSource) {
                    entry.roleSource = [];
                }
                entry.roleSource.push(roleElement.role);
                return entry;
            });
            output = _lodash2.default.concat(output, subRole);
        } else {
            output.push(roleElement);
        }
    });

    return output;
}