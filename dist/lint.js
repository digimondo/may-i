'use strict';

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _check = require('./check');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function lint(role) {
    var res = {
        errors: []
    };

    //duplicate check
    for (var i = 0; i < role.length; i++) {
        var perm1 = role[i];
        var scope1 = _lodash2.default.keys(perm1)[0];
        for (var j = i + 1; j < role.length; j++) {
            var perm2 = role[j];
            var scope2 = _lodash2.default.keys(perm2)[0];
            if ((0, _check.matches)(perm2, perm1)) {
                res.errors.push({
                    message: "Scope is overwritten by other scope",
                    overwrittenScope: scope1,
                    overwritingScope: scope2
                });
            }
        }
    }

    return res;
}

module.exports = {
    lint: lint
};