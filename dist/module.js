"use strict";

module.exports = {
    compile: require('./compile'),
    check: require('./check'),
    lint: require('./lint')
};