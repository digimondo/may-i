import {compileQuery, check, matches} from '../lib/check';
import expect from 'unexpected';

describe('check', function () {
    describe('compileQuery()', function () {
        it('should put params into the query', function () {
            expect(compileQuery('cat/:id/view', {id: 2}), 'to equal', 'cat/2/view');
            expect(compileQuery('cat/create', {id: 2}), 'to equal', 'cat/create');
            expect(compileQuery('cat/:catId/photo/:photoId/view', {catId:2, photoId: 10}), 'to equal', 'cat/2/photo/10/view');
        })
    });

    describe('matches()', function() {
        it('should match if query is equal to scope', function() {
            expect(matches('cat/create', 'cat/create'), 'to be true');
            expect(matches('cat/create', 'cat/10/view'), 'to be false');
            expect(matches('cat/create', 'cat/list'), 'to be false');
        });
        it('should match if the query is more specific than the scope', function() {
            expect(matches('cat', 'cat/create'), 'to be true');
            expect(matches('cat/create', 'cat'), 'to be false');
        });
        it('should match wildcards', function() {
            expect(matches('cat/*/view', 'cat/10/view'), 'to be true');
            expect(matches('cat/*/view', 'cat/18/view'), 'to be true');
            expect(matches('cat/*', 'cat/create'), 'to be true');
            expect(matches('cat/*', 'cat/10/view'), 'to be true');
        })
    });

    describe('check()', function () {
        var role = [
            {'cat': true},
            {'cat/*/view': true},
            {'cat/create': false},
            {'cat/*/edit': false},
            {'cat/10/edit': true}
        ];
        it('should work without params', function() {
            expect(check(role, 'cat/11/edit'), 'to be false');
            expect(check(role, 'cat/10/edit'), 'to be true');
            expect(check(role, 'cat/10/view'), 'to be true');
            expect(check(role, 'cat/create'), 'to be false');
        });

        it('should work with params', function() {
            expect(check(role, 'cat/:catId/edit', {catId: 10}), 'to be true');
            expect(check(role, 'cat/:catId/edit', {catId: 11}), 'to be false');
        });
    });
});
