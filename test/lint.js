import expect from 'unexpected';
import {lint} from '../lib/lint';

describe("lint", function() {
    it('should detect invalidated scopes', function() {
        var role = [
            {'cat/create':true},
            {'cat': false}
        ];
        expect(lint(role).errors, 'to be non-empty');
    })
});