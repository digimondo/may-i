"use strict";

var expect = require('unexpected');
import {compile} from '../lib/compile';

describe('compile(role, roleProvider)', function () {
    it('should return all permissions from the given role', function () {
        var role = [
            {'test/this/shit': true},
            {'test/*': false},
            {'test/:id/create': true}
        ];
        expect(compile(role), "to equal", role);
    });

    it('should inject roles', function() {
        var role = [
            {role: 'default'},
            {'test/this': true}
        ];
        var roleProvider = function(name) {
            switch(name) {
                case 'default':
                    return [
                        {'*': false},
                        {'test': true}
                    ];
            }
        };

        expect(compile(role, roleProvider), "to equal", [
            {'*': false},
            {'test': true},
            {'test/this': true}
        ])
    });

    it('should inject roles recursively', function() {
        var role = [
            {role: 'admin'}
        ];
        var roleProvider = function(name) {
            switch (name) {
                case 'admin':
                    return [
                        {role: 'default'},
                        {'test': true}
                    ];
                case 'default':
                    return [
                        {'*': false}
                    ]
            }
        };
        expect(compile(role, roleProvider), "to equal", [
            {'*': false},
            {'test': true}
        ])
    })
});
